Django bootstrap est un kit de démarrage rapide permettant de créer un projet Django dans un environnement Docker très simplement. 

Le kit dans sa version de base inclus :
* Python 3
* postgres
* Django (>=2.0 && <3.0)

Le projet requiert l'installation préalable de [Docker](https://www.docker.com/get-started) et [Compose](https://docs.docker.com/compose/install/).

## Installation

``` shell
git clone git@gitlab.com:valkiki/django-bootstrap.git mon_projet_de_test
```

Dans le projet : 
``` shell 
docker-compose run web django-admin startproject mon_projet_de_test .
```
``` shell 
docker-compose up
``` 
Rendez-vous ensuite sur http://localhost:8000 pour véirifer l'installation.

## Utilisation
L'accès aux commandes python se font via le bash du container Docker :
``` shell
docker exec -it mon_projet_de_test_web_1 bash
```
